#! /usr/bin/env python3

import sys, os
from collections import defaultdict
import statistics as stats
import plotly.graph_objects as go

plot_title = None
legend = None

colors = {
    "bm": "gray",
    "oc": "lightcoral",
}

rename = {
    "bm": "BareMetal",
    "oc": "OpenShift",
}

plot_title = "SysBench CPU benchmark (higher is better)"

X = defaultdict(lambda:defaultdict(list))

for fpath in sys.argv[1:]:
    fname = os.path.basename(fpath)

    # fname: <platform>_sys-cpu.<worker>_<threads>threads.<run>
    run = fname.rpartition(".")[-1]

    platform = fname.partition("_")[0]
    threads = fname.split("_")[-1].partition(".")[0]
    worker = fname.split("_")[1].split(".")[-1]


    with open(fpath) as f:
        evt_per_sec = None
        for _line in f:
            line = _line.strip()
            if line.startswith("events per second"):
                evt_per_sec = float(line.split(":")[-1].strip())

    if evt_per_sec is None :
        #print(f"Failed to parse {fpath}: avg_lat={avg_lat}; evt_per_sec={evt_per_sec}")
        continue

    X[f"{platform}"][f"{threads}"].append(evt_per_sec)
data = []
for platform_name, platform_values in X.items():
    x = []
    y = []
    err = []
    data_length=None
    for x_thread_value, y_cpu_values in platform_values.items():
        x.append(x_thread_value)
        y.append(stats.mean(y_cpu_values))
        stdev = stats.stdev(y_cpu_values) if len(y_cpu_values) > 2 else 0
        err.append(stdev)
        data_length = len(y_cpu_values)

    name = rename[platform_name]
    ADD_DETAILS = False
    if ADD_DETAILS:
        name += f"({data_length} measures)"
    data.append(go.Bar(name=name,
                       x=x, y=y,
                       marker_color=colors.get(platform_name),
                       legendgroup=platform_name,
                       error_y=dict(type='data', array=err),
                       ))


fig = go.Figure(data=data)

fig.update_layout(legend=dict(
    yanchor="top",
    y=0.99,
    xanchor="left",
    x=0.01
))

# Edit the layout
#x_title, y_title = legend.split(maxsplit=1)
fig.update_layout(title=plot_title, title_x=0.5,
                  yaxis_title="Events per second. Higher is better.")

fig.show()
