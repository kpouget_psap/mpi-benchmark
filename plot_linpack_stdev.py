#! /usr/bin/env python3

import sys, os
from collections import defaultdict
import statistics as stats
import plotly.graph_objects as go

plot_title = None
legend = None

colors = {
    "bm": "blue",
    "oc": "red",
}

plot_title = "Linpack"

X = defaultdict(lambda:defaultdict(list))

for fpath in sys.argv[1:]:
    fname = os.path.basename(fpath)

    # fname: bm_<platform>.<nthreads>threads
    platform, _, threads = fname.partition(".")

    in_summary = False
    is_next = False

    with open(fpath) as f:
        for _line in f:
            line = _line.strip()
            if "Performance Summary" in line:
                in_summary = True
                continue
            elif not in_summary:
                continue
            if line.startswith("Size"):
                is_next = True
                continue
            if not is_next:
                continue
            if not line:
                break
            # Size   LDA    Align.  Average  Maximal
            # 20000  20016  4       42.8123  42.8123
            size, lda, align, avg, maxi = line.split()

            X[f"{platform}"][f"{threads}"].append([float(avg), float(maxi)])

data = []
for platform_name, platform_values in X.items():
    x = []
    y = []
    err = []
    data_length=None
    for x_thread_value, y_cpu_values in platform_values.items():
        avg, maxi = y_cpu_values[0]
        x.append(x_thread_value)
        y.append(avg)
        err.append(avg-maxi)

    data.append(go.Scatter(name=f"{platform_name}",
                       x=x, y=y,
                       marker_color=colors.get(platform_name),
                       legendgroup=platform_name,
                       error_y=dict(type='data', array=err),
                       ))


fig = go.Figure(data=data)

# Edit the layout
#x_title, y_title = legend.split(maxsplit=1)
fig.update_layout(title=plot_title,
                  yaxis_title="GFlops. Higher is better.")

fig.show()
