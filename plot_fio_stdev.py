#! /usr/bin/env python3

import sys, os
from collections import defaultdict
import statistics as stats
import plotly.subplots
import plotly.graph_objects as go

WRITE_LOWER_LIMIT = 125

plot_title = None
legend = None

COLORS = {
    "bm-local": "gray",
    "oc-local": "lightcoral",
    "oc-overlay": "coral",
    #"oc-tmp": "green",
    "bm-cephfs": "blue",
    "oc-cephfs": "lightblue",
}

rename = {
    "bm-local": "Baremetal",
    "oc-local": "OpenShift local storage",
    "oc-overlay": "OpenShift overlay",
}

plot_title = "SysBench FIO read&write throughput. Higher is better."


Y = {"read": defaultdict(list),
     "write": defaultdict(list)}
Y_skipped = defaultdict(int)

legend_names = []
for fpath in sys.argv[1:]:
    fname = os.path.basename(fpath)

    # fname: <platform>_sys-fio_<FS>.<worker>_<threads>threads.<run>
    run = fname.rpartition(".")[-1]
    #if run == "2": continue

    platform = fname.partition("_")[0]
    fs = fname.split(".")[0].split("_")[-1]
    worker = fname.split("_")[2].split(".")[-1]

    legend_name =  f"{platform}-{fs}"

    with open(fpath) as f:
        thput_read = None
        thput_write = None

        current_section = None
        for _line in f:
            line = _line.strip()
            if not _line.startswith(" "):
                current_section = line[:-1]
                continue
            key, _, _val = line.partition(":")
            if current_section == "Throughput":
                val = float(_val.strip())
                if val == 0.0: continue
                if key == "written, MiB/s":
                    if val > WRITE_LOWER_LIMIT:
                        thput_write = val
                    else:
                        print(f"{fpath} skipped ({val} MiB/s)")
                        pass # skip, node disk is under usual performance

                if key == "read, MiB/s":
                    thput_read = val

    if None in (thput_read, thput_write) :
        Y_skipped[legend_name] += 1
        continue

    Y["read"][legend_name].append(thput_read)
    Y["write"][legend_name].append(thput_write)
    legend_names.append(legend_name)


data = []

X = {"read": "Read",
     "write": "Write"}

fig = plotly.subplots.make_subplots(
    rows=1, cols=2,
    specs=[[{"secondary_y": False}, {"secondary_y": True}]])

for what in "read", "write":
    for legend_name in COLORS.keys(): # to get the right order
        if legend_name not in legend_names: continue
        x = []
        y = []
        err = []

        x.append(X[what])

        nb_measurements = len(Y[what][legend_name])
        y.append(stats.mean(Y[what][legend_name]))
        err.append(stats.stdev(Y[what][legend_name]))

        name = rename.get(legend_name, legend_name)
        ADD_DETAILS = False
        if ADD_DETAILS:
            name += f" ({nb_measurements} measures + {Y_skipped[legend_name]} skipped)"
        fig.add_trace(go.Bar(name=name,
                             marker_color=COLORS[legend_name],
                             hoverlabel= {'namelength' :-1},
                             legendgroup=legend_name,
                             showlegend=what == "write",
                             error_y=dict(type='data',
                                          array=err),
                             x=x, y=y),
                      secondary_y=what == "write",
                      row=1, col=1 if what == "read" else 2)

fig.update_layout(legend=dict(
    yanchor="middle",
    y=0,
    xanchor="center",
    x=0.47
))

fig.update_yaxes(title_text="Read throughput (in MiB/s)", secondary_y=False)
fig.update_yaxes(title_text="Write throughput (in MiB/s)", secondary_y=True)
# Edit the layout
fig.update_layout(title=plot_title, title_x=0.5,
                  xaxis_title="")

fig.show()
