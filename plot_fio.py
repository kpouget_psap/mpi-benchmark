#! /usr/bin/env python3

import sys, os
from collections import defaultdict

import plotly.graph_objects as go

WRITE_LOWER_LIMIT = 0

plot_title = None
legend = None

colors = {
    "bm-cephfs": "blue",
    "bm-local": "red",
    "oc-overlay": "purple",
    "oc-local": "orange",
    "oc-tmp": "green",
    "oc-cephfs": "lightblue",
}

plot_title = "SysBench/FIO"


C = dict()
G = dict()
X = defaultdict(list)
Y_thput_write = defaultdict(list)
Y_thput_read = defaultdict(list)

for fpath in sys.argv[1:]:
    fname = os.path.basename(fpath)

    # fname: <platform>_sys-fio_<FS>.<worker>_<threads>threads.<run>
    run = fname.rpartition(".")[-1]
    #if run == "2": continue

    platform = fname.partition("_")[0]
    fs = fname.split(".")[0].split("_")[-1]
    worker = fname.split("_")[2].split(".")[-1]

    legend_name =  f"{platform}-{fs} | {worker}"

    legend_grp = f"{platform}-{fs}"
    x_name = f"run #{run}"

    with open(fpath) as f:
        thput_read = None
        thput_write = None

        current_section = None
        for _line in f:
            line = _line.strip()
            if not _line.startswith(" "):
                current_section = line[:-1]
                continue
            key, _, _val = line.partition(":")
            if current_section == "Throughput":
                val = float(_val.strip())
                if val == 0.0: continue
                if key == "written, MiB/s":
                    if val > WRITE_LOWER_LIMIT:
                        thput_write = val
                    else:
                        print(f"{fpath} skipped ({val} MiB/s)")
                        pass # skip, node disk is under usual performance

                if key == "read, MiB/s":
                    thput_read = val

    if None in (thput_read, thput_write) :
        continue

    X[legend_name].append(x_name)
    G[legend_name] = legend_grp
    C[legend_name] = colors[f"{platform}-{fs}"]
    Y_thput_read[legend_name].append(thput_read)
    Y_thput_write[legend_name].append(thput_write)


def plot(Y, y_title):
    data = []

    for legend_name in sorted(X.keys()):
        x = X[legend_name]
        y = Y[legend_name]
        data.append(go.Bar(name=legend_name,
                           marker_color=C[legend_name],
                           legendgroup=G[legend_name],
                           hoverlabel= {'namelength' :-1},
                           x=x, y=y,
                           ))

    fig = go.Figure(data=data)

    # Edit the layout
    y_title = y_title
    x_title = ""
    fig.update_layout(title=plot_title + " " + y_title,
                      xaxis_title=x_title,
                      yaxis_title=y_title)

    fig.show()

plot(Y_thput_read, "Throughput [Read], in MiB/s. Higher is better")
plot(Y_thput_write, "Throughput [Write], in MiB/s. Higher is better")
