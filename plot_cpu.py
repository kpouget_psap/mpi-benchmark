#! /usr/bin/env python3

import sys, os
from collections import defaultdict

import plotly.graph_objects as go

plot_title = None
legend = None

colors = {
    "bm": "blue",
    "oc": "red",
}

plot_title = "SysBench/CPU"


C = dict()
G = dict()
X = defaultdict(list)
Y_evt_per_sec = defaultdict(list)
Y_avg_lat = defaultdict(list)

for fpath in sys.argv[1:]:
    fname = os.path.basename(fpath)
    
    # fname: <platform>_sys-cpu.<worker>_<threads>threads.<run>
    run = fname.rpartition(".")[-1]

    platform = fname.partition("_")[0]
    threads = fname.split("_")[-1].partition(".")[0]
    worker = fname.split("_")[1].split(".")[-1]

    key =  f"{platform} {worker}"
    grp = f"{platform}"
    x_name = f"{threads}"

    with open(fpath) as f:
        evt_per_sec = None
        avg_lat = None
        for _line in f:
            line = _line.strip()
            if line.startswith("events per second"):
                evt_per_sec = float(line.split(":")[-1].strip())
            elif line.startswith("avg:"):
                avg_lat = float(line.split(":")[-1].strip())
            
    if None in (avg_lat, evt_per_sec) :
        print(f"Failed to parse {fpath}: avg_lat={avg_lat}; evt_per_sec={evt_per_sec}")
        continue
    
    X[key].append(x_name)
    G[key] = grp
    C[key] = colors[platform]
    Y_evt_per_sec[key].append(evt_per_sec)
    Y_avg_lat[key].append(avg_lat)



def plot(Y, y_title):
    data = []

    for key in sorted(X.keys()):
        x = X[key]
        y = Y[key]
        data.append(go.Bar(name=key,
                           marker_color=C[key],
                           #legendgroup=G[key],
                           x=x, y=y,
                           ))

    fig = go.Figure(data=data)

    # Edit the layout
    y_title = y_title
    x_title = ""
    fig.update_layout(title=plot_title + " " + y_title,
                      xaxis_title=x_title,
                      yaxis_title=y_title)

    fig.show()

plot(Y_evt_per_sec, "events per second. Higher is better")
#plot(Y_avg_lat, "average latency (in ms). Lower is better")
