#! /usr/bin/env python3

import plotly.graph_objects as go
import sys

plot_title = None
legend = None
data = []

colors = {
    "baremetal": "black",
    "Multus": "red",
    "SDN": "blue",
    "HostNetwork":"green"
}

for fname in sys.argv[1:]:
    X = []
    Y = []
    with open(fname) as f:
        for line in f:
            if not line.strip(): continue
            if line.startswith('#'):
                if plot_title is None:
                    plot_title = line[1:].strip()
                elif legend is None:
                    legend = line[1:].strip()
                continue
            x, y = line.strip().split()
            X.append(float(x))
            Y.append(float(y))
    title = fname.partition(".")[-1]

    try:
        net_name = title.split(".")[-2]
    except IndexError:
        net_name = title.split(".")[-1]
    if "All-to-All" in plot_title:
        net_name = title.split(".")[0]

    data.append(go.Scatter(name=title,
                           x=X, y=Y,
                           hoverlabel= {'namelength' :-1},
                           line=dict(color=colors.get(net_name)),
                           #legendgroup=net_name
                           ))

fig = go.Figure(data=data)

# Edit the layout
x_title, y_title = legend.split(maxsplit=1)
fig.update_layout(title=plot_title,
                   xaxis_title=x_title,
                   yaxis_title=y_title)

fig.show()
