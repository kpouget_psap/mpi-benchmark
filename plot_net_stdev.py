#! /usr/bin/env python3

from collections import defaultdict
import plotly.graph_objects as go
import sys
import statistics as stats

plot_title = None
legend = None
data = []

colors = {
    "baremetal": "black",
    "Multus": "red",
    "SDN": "blue",
    "HostNetwork":"green"
}

colors_nodes = {
    "4nodes": "black",
    "8nodes": "red",
    "12nodes": "blue",
    "16nodes":"orange",
    "20nodes":"yellow",
    "25nodes":"purple",
    "28nodes":"lightblue",
    "32nodes":"black",
}

X = defaultdict(lambda:defaultdict(list))
skipped = defaultdict(int)

for fname in sys.argv[1:]:
    if "osu-allreduce" in fname or "osu-alltoall" in fname:
        _, net, nodes, run = fname.split(".")
        net_name = f"{net} {nodes}"
    else:
        title = fname.partition(".")[-1]
        try:
            net_name = title.split(".")[-2]
        except IndexError:
            net_name = title.split(".")[-1]

    with open(fname) as f:
        for line in f:
            if not line.strip(): continue
            if line.startswith('#'):
                if plot_title is None:
                    plot_title = line[1:].strip()
                    if "osu-allreduce" in fname:
                        plot_title += " (allreduce)"
                elif legend is None:
                    legend = line[1:].strip()
                continue
            else:
                x, y = line.strip().split()
                X[net_name][x].append(float(y))
        if "Bandwidth" in plot_title:
            if X[net_name][x][-1] < 1000: #MB/s
                for values in X[net_name].values():
                    values.pop()
                skipped[net_name] += 1

def sort_key(k):
    if not "All-to-All" in plot_title: return k

    _net, _nodes = k.split()
    _nodes = int(_nodes.replace("nodes", ""))
    return f"{_net} {_nodes:03}nodes"

for net_name in sorted(X, key=sort_key):

    net_values = X[net_name]
    x = []
    y = []
    err_pos = []
    err_neg = []
    nb_measurements = None
    for x_value, y_values in net_values.items():
        x.append(float(x_value))
        y.append(stats.mean(y_values))
        err_pos.append(y[-1] + (stats.stdev(y_values) if len(y_values) > 2 else 0))
        err_neg.append(y[-1] - (stats.stdev(y_values) if len(y_values) > 2 else 0))
        nb_measurements = len(y_values)

    legend_name = f"{net_name}"

    ADD_DETAILS = False
    if ADD_DETAILS:
        legend_name += f" ({nb_measurements} measures"

        if skipped[net_name]:
            legend_name += f" + {skipped[net_name]} skipped"
        legend_name += ")"

    if "All-to-All" in plot_title or "Allreduce" in plot_title:
        color = colors.get(net_name.split()[0])
        #color = colors_nodes.get(net_name.split()[1])
        if color is None: import pdb;pdb.set_trace()
    else:
        color = colors.get(net_name)

    data.append(go.Scatter(name=legend_name,
                           x=x, y=y,
                           mode="markers+lines",
                           hoverlabel= {'namelength' :-1},
                           line=dict(color=color, width=1),
                           legendgroup=net_name
                           ))

    data.append(go.Scatter(name=legend_name+color,
                           x=x, y=err_pos,
                           line=dict(color=color, width=2),
                           legendgroup=net_name,
                           showlegend=False,
                           ))
    data.append(go.Scatter(name=legend_name+color,
                           x=x, y=err_neg,
                           showlegend=False,
                           fill='tonexty',
                           line=dict(color=color, width=2),
                           legendgroup=net_name
                           ))

fig = go.Figure(data=data)

if "MPI Latency" in plot_title:
    plot_title = "OSU MPI Latency Test (lower is better)"
elif "Bandwidth" in plot_title:
    plot_title = "OSU MPI Bandwidth Test (higher is better)"
elif "All-to-All" in plot_title:
    plot_title = "OSU MPI All-to-All Latency Test (lower is better)"

# Edit the layout
x_title, y_title = legend.split(maxsplit=1)
fig.update_layout(title=plot_title, title_x=0.5,
                   xaxis_title="Message "+x_title,
                   yaxis_title=y_title)

if "All-to-All" in plot_title:
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.5,
        xanchor="left",
        x=0.5
    ))
else:
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.5,
        xanchor="left",
        x=0.85
    ))

fig.show()
